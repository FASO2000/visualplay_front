$(async function() {
    await iniciar_carga()
    const valores = window.location.search;
    const urlParams = new URLSearchParams(valores);
    var id_video = urlParams.get('id_video');

    await fetch('http://localhost:3000/load_video', {
            method: 'POST',
            body: JSON.stringify({
                id_video: id_video
            }),
            headers: {
                "Content-type": "application/json"
            }
        })
        .then(response => response.json())
        .then(json => load_video(json['data']))
    await finalizar_carga()

});

function load_video(item) {

    html = '<div class="title_div text-center">'
    html += '<h1 class="text">' + item['title'] + '</h1>'
    html += '</div>'
    html += '<video controls style="margin: auto;"  height="600">'
    html += '<source type="video/mp4" src="data:' + item['mime_video'] + ';base64,' + item['video'] + '">'
    html += '</video>'
    html += '<div class="description_div text-left">'
    html += '<p class="text">' + item['description'] + '</p>'
    html += '<h3>Comentarios</h3>'
    html += '</div>'
        // html += '<div class="comment_div text-left">'
        // html += '</div>'

    $("#div_video").append(html);
}