$(async function() {
    await iniciar_carga();
    await fetch('http://localhost:3000/view_videos_principal')
        .then(res => res.json())
        .then(res => datos(res))
        .catch(err => console.error(err));
    await finalizar_carga();
});


function datos(res) {

    html = "";
    $.each(res['data'], function(key, value) {

        html += '<div class="card">'
        html += '<a href="visualizar.html?id_video=' + value['id'] + '">'
        html += '<img id="' + value['name'] + '" src="data:' + value['mime_image'] + ';base64,' + value['miniature'] + '">'
        html += '<h4>' + value['title'] + '</h4>'
        html += '</a>'
        html += '</div>'

    });


    $("#list_videos").append(html);

}