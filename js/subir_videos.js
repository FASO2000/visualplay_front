var bs64Miniatura;
var bs64Video;
var fileInput = document.querySelector('#subir');
var miniature = document.querySelector('#mini');

fileInput.addEventListener("change", (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
        const base64String = reader.result.replace("data:", "").replace(/^.+,/, "");

        bs64Video = base64String
    };
    reader.readAsDataURL(file);
});

miniature.addEventListener("change", (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
        const base64String = reader.result.replace("data:", "").replace(/^.+,/, "");

        bs64Miniatura = base64String
    };
    reader.readAsDataURL(file);
});

function upload() {

    var title = document.getElementById('titulo').value;
    var description = document.getElementById('descripcion').value;
    name_ = title.toLowerCase().replace(/ /g, "")
    resp = false;


    if (bs64Miniatura != undefined && bs64Video != undefined) {
        var mime = document.querySelector('#subir').files[0].type;
        var mime_min = document.querySelector('#mini').files[0].type;

        fetch('http://localhost:3000/upload_video', {
                method: 'POST',
                body: JSON.stringify({
                    file: bs64Video,
                    name: name_,
                    mime: mime,
                    mime_min: mime_min,
                    title: title,
                    miniature: bs64Miniatura,
                    description: description
                }),
                headers: {
                    "Content-type": "application/json"
                }
            })
            .then(response => response.json())
            .then(function(json) {
                resp = alert_error(json)
                if (resp) {
                    limpiar()
                }
            })
    } else {
        alert("llenar los campos de video e imagen")
    }

}



function limpiar() {
    document.getElementById('subir').value = "";
    document.getElementById('titulo').value = "";
    document.getElementById('mini').value = "";
    document.getElementById('descripcion').value = "";

    window.location.href = "subirVideos.html";
}