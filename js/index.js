function loguear() {
    email = document.getElementById('email').value;
    password = document.getElementById('password').value;
    action = 1;
    resp = false;

    fetch('http://localhost:3000/login', {
            method: 'POST',
            body: JSON.stringify({
                email: email,
                password: password,
                action: action
            }),
            headers: {
                "Content-type": "application/json"
            }
        })
        .then(response => response.json())
        .then(function(json) {
            resp = alert_error(json)
            if (resp) {
                limpiar()
            }
        })
}

function limpiar() {
    console.log("")
    document.getElementById('email').value = "";
    document.getElementById('password').value = "";
    window.location.href = "inicio.html";
}