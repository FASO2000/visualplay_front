function alert(message, type) {
    console.log(message, " ", type)
    var wrapper = document.createElement('div')
    var alertPlaceholder = document.getElementById('alert')
    wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

    alertPlaceholder.append(wrapper)
}

function alert_error(response) {
    if (response['status_code'] == 400) {
        alert(response['messagge'], 'danger')
        return false;
    } else {
        return true;
    }
}