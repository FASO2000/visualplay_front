function registrar() {
    email = document.getElementById('email').value;
    password = document.getElementById('password').value;
    nombre = document.getElementById('nombre').value;
    username = document.getElementById('usuario').value;
    console.log(email, " ", password, " ", nombre, " ", username)
    resp = false;

    fetch('http://localhost:3000/create_user', {
            method: 'POST',
            body: JSON.stringify({
                email: email,
                password: password,
                nombre: nombre,
                username: username
            }),
            headers: {
                "Content-type": "application/json"
            }
        })
        .then(response => response.json())
        .then(function(json) {
            resp = alert_error(json)
            if (resp) {
                $("#exampleModal").modal('show');
            }
        })
}

function limpiar() {
    document.getElementById('email').value = "";
    document.getElementById('password').value = "";
    document.getElementById('nombre').value = "";
    document.getElementById('usuario').value = "";
}

function verificar() {
    email = document.getElementById('email').value;
    code = document.getElementById('codigo').value;

    fetch('http://localhost:3000/confir_user', {
            method: 'POST',
            body: JSON.stringify({
                email: email,
                code: code
            }),
            headers: {
                "Content-type": "application/json"
            }
        })
        .then(response => response.json())
        .then(function(json) {
            resp = alert_error(json)
            if (resp) {
                limpiar()
                window.location.href = "index.html";
            }
        })
}